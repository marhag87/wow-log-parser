extern crate nom;
extern crate wow_log_parser;

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::{env, io};
use wow_log_parser::events::{combatant_info, encounter_start, spell_aura_applied};
use wow_log_parser::items::{item_name, item_quality};
use wow_log_parser::{datetime, event_type, EventType};

fn get_file_content(filename: &str) -> io::Result<BufReader<File>> {
    let file = File::open(filename)?;
    Ok(BufReader::new(file))
}

fn parse_file(filename: &str) {
    let mut encounter = String::from("");
    let mut encounter_time = String::from("0");
    let mut combatant_infos = Vec::new();
    let mut player_map = HashMap::new();
    if let Ok(file) = get_file_content(filename) {
        for line in file.lines() {
            if let Ok((rest, datetime)) = datetime(line.unwrap().as_str()) {
                if let Ok((rest, event_type)) = event_type(rest) {
                    match event_type {
                        EventType::CombatantInfo => {
                            if let Ok((_, combatant_info)) = combatant_info(rest) {
                                combatant_infos.push((
                                    combatant_info,
                                    encounter.clone(),
                                    encounter_time.clone(),
                                ));
                            }
                        }
                        EventType::EncounterStart => {
                            if let Ok((_, encounter_start)) = encounter_start(rest) {
                                encounter = encounter_start.name;
                                encounter_time = datetime.timestamp().to_string();
                            }
                        }
                        EventType::SpellAuraApplied => {
                            if let Ok((_, spell_aura_applied)) = spell_aura_applied(rest) {
                                player_map.insert(
                                    spell_aura_applied.guid.clone(),
                                    spell_aura_applied.name.clone(),
                                );
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
    }

    combatant_infos
        .iter()
        .for_each(|(combatant_info, encounter, encounter_time)| {
            let items = combatant_info
                .items
                .iter()
                .map(|item| {
                    if let Some(item) = item {
                        format!("{};{:?}", item_name(item.id), item_quality(item.id))
                    } else {
                        format!(";")
                    }
                })
                .collect::<Vec<String>>();
            println!(
                "{};{};{};{}",
                encounter_time,
                encounter,
                player_map.get(combatant_info.guid.as_str()).unwrap(),
                items.join(";")
            );
        });
}

fn main() {
    let args: Vec<String> = env::args().collect();
    match args.len() {
        2 => parse_file(args[1].as_str()),
        _ => eprintln!("You need to supply a filename"),
    }
}
