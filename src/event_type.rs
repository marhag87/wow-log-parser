use nom::bytes::complete::{tag, take_until};
use nom::combinator::map;
use nom::IResult;

#[derive(Debug, PartialEq)]
pub enum EventType {
    CombatLogVersion,
    SpellAuraApplied,
    EnvironmentalDamage,
    SpellCastSuccess,
    SpellSummon,
    SpellPeriodicEnergize,
    UnitDestroyed,
    SpellAuraAppliedDose,
    SpellPeriodicDamage,
    SpellAuraRemoved,
    SpellDispel,
    SpellCastStart,
    SpellHeal,
    UnitDied,
    SpellResurrect,
    SpellPeriodicHeal,
    SpellAuraRefresh,
    SpellEnergize,
    RangeDamage,
    SpellCastFailed,
    SwingDamage,
    SwingDamageLanded,
    SpellDamage,
    DamageShield,
    SpellExtraAttacks,
    SpellMissed,
    SpellAuraRemovedDose,
    SwingMissed,
    SpellAuraBrokenSpell,
    DamageShieldMissed,
    SpellAbsorbed,
    RangeMissed,
    PartyKill,
    SpellInstakill,
    EncounterStart,
    CombatantInfo,
    EncounterEnd,
    SpellPeriodicMissed,
    SpellInterrupt,
    SpellCreate,
    SpellDrain,
    SpellAuraBroken,
    SpellDurabilityDamage,
    EnchantApplied,
    Unknown,
}

impl From<&str> for EventType {
    fn from(text: &str) -> Self {
        use EventType::*;
        match text {
            "COMBAT_LOG_VERSION" => CombatLogVersion,
            "SPELL_AURA_APPLIED" => SpellAuraApplied,
            "ENVIRONMENTAL_DAMAGE" => EnvironmentalDamage,
            "SPELL_CAST_SUCCESS" => SpellCastSuccess,
            "SPELL_SUMMON" => SpellSummon,
            "SPELL_PERIODIC_ENERGIZE" => SpellPeriodicEnergize,
            "UNIT_DESTROYED" => UnitDestroyed,
            "SPELL_AURA_APPLIED_DOSE" => SpellAuraAppliedDose,
            "SPELL_PERIODIC_DAMAGE" => SpellPeriodicDamage,
            "SPELL_AURA_REMOVED" => SpellAuraRemoved,
            "SPELL_DISPEL" => SpellDispel,
            "SPELL_CAST_START" => SpellCastStart,
            "SPELL_HEAL" => SpellHeal,
            "UNIT_DIED" => UnitDied,
            "SPELL_RESURRECT" => SpellResurrect,
            "SPELL_PERIODIC_HEAL" => SpellPeriodicHeal,
            "SPELL_AURA_REFRESH" => SpellAuraRefresh,
            "SPELL_ENERGIZE" => SpellEnergize,
            "RANGE_DAMAGE" => RangeDamage,
            "SPELL_CAST_FAILED" => SpellCastFailed,
            "SWING_DAMAGE" => SwingDamage,
            "SWING_DAMAGE_LANDED" => SwingDamageLanded,
            "SPELL_DAMAGE" => SpellDamage,
            "DAMAGE_SHIELD" => DamageShield,
            "SPELL_EXTRA_ATTACKS" => SpellExtraAttacks,
            "SPELL_MISSED" => SpellMissed,
            "SPELL_AURA_REMOVED_DOSE" => SpellAuraRemovedDose,
            "SWING_MISSED" => SwingMissed,
            "SPELL_AURA_BROKEN_SPELL" => SpellAuraBrokenSpell,
            "DAMAGE_SHIELD_MISSED" => DamageShieldMissed,
            "SPELL_ABSORBED" => SpellAbsorbed,
            "RANGE_MISSED" => RangeMissed,
            "PARTY_KILL" => PartyKill,
            "SPELL_INSTAKILL" => SpellInstakill,
            "ENCOUNTER_START" => EncounterStart,
            "COMBATANT_INFO" => CombatantInfo,
            "ENCOUNTER_END" => EncounterEnd,
            "SPELL_PERIODIC_MISSED" => SpellPeriodicMissed,
            "SPELL_INTERRUPT" => SpellInterrupt,
            "SPELL_CREATE" => SpellCreate,
            "SPELL_DRAIN" => SpellDrain,
            "SPELL_AURA_BROKEN" => SpellAuraBroken,
            "SPELL_DURABILITY_DAMAGE" => SpellDurabilityDamage,
            "ENCHANT_APPLIED" => EnchantApplied,
            _ => {
                eprintln!("Could not parse event: {}", text);
                Unknown
            }
        }
    }
}

pub fn event_type(input: &str) -> IResult<&str, EventType> {
    let (input, event_type) = map(take_until(","), EventType::from)(input)?;
    let (input, _) = tag(",")(input)?;
    Ok((input, event_type))
}

#[test]
fn parse_combatant_info() {
    assert_eq!(
        event_type("COMBATANT_INFO,"),
        Ok(("", EventType::CombatantInfo))
    )
}
#[test]
fn parse_spell_cast_success() {
    assert_eq!(
        event_type("SPELL_CAST_SUCCESS,"),
        Ok(("", EventType::SpellCastSuccess))
    )
}
