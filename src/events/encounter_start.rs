use crate::common::num;
use nom::bytes::complete::{tag, take_until};
use nom::sequence::tuple;
use nom::IResult;

#[derive(Debug, PartialEq)]
pub struct EncounterStart {
    pub id: u32,
    pub name: String,
    pub difficulty: u32,
    pub group_size: u32,
    pub unknown: u32,
}

pub fn encounter_start(input: &str) -> IResult<&str, EncounterStart> {
    let (input, (id, _, name, _, difficulty, group_size, unknown)) =
        tuple((num, tag("\""), take_until("\""), tag("\","), num, num, num))(input)?;
    Ok((
        input,
        EncounterStart {
            id,
            name: String::from(name),
            difficulty,
            group_size,
            unknown,
        },
    ))
}

#[test]
fn can_parse_encounter_start() {
    assert_eq!(
        encounter_start("1084,\"Onyxia\",9,40,249"),
        Ok((
            "",
            EncounterStart {
                id: 1084,
                name: String::from("Onyxia"),
                difficulty: 9,
                group_size: 40,
                unknown: 249,
            }
        ))
    )
}

#[test]
fn can_parse_with_space() {
    assert_eq!(
        encounter_start("668,\"Baron Geddon\",9,40,409"),
        Ok((
            "",
            EncounterStart {
                id: 668,
                name: String::from("Baron Geddon"),
                difficulty: 9,
                group_size: 40,
                unknown: 409,
            }
        ))
    )
}
