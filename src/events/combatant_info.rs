use crate::common::num;
use nom::bytes::complete::{tag, take_until};
use nom::combinator::{map_res, opt};
use nom::multi::many0;
use nom::sequence::tuple;
use nom::IResult;

#[derive(Debug)]
pub struct CombatantInfo {
    pub guid: String,
    pub strength: u32,
    pub agility: u32,
    pub stamina: u32,
    pub intelligence: u32,
    pub spirit: u32,
    pub dodge: u32,
    pub parry: u32,
    pub block: u32,
    pub critmelee: u32,
    pub critranged: u32,
    pub critspell: u32,
    pub speed: u32,
    pub lifesteal: u32,
    pub hastemelee: u32,
    pub hasteranged: u32,
    pub hastespell: u32,
    pub avoidance: u32,
    pub mastery: u32,
    pub versatilitydamagedone: u32,
    pub versatilityhealingdone: u32,
    pub versatilitydamagetaken: u32,
    pub armor: u32,
    pub currentspecid: u32,
    pub class_talents: Vec<u32>,
    pub pvp_talents: Vec<u32>,
    pub artifact_traits: Vec<u32>,
    pub items: Vec<Option<Item>>,
    pub interesting_auras: Vec<Aura>,
}

#[derive(Debug, PartialEq)]
pub struct Item {
    pub id: u32,
    pub level: u32,
    pub permanent_enchant_id: Option<u32>,
    pub temporary_enchant_id: Option<u32>,
    pub on_use_spell_enchant_id: Option<u32>,
    pub bonus_list_ids: Vec<u32>,
    pub gems: Vec<Gem>,
}

#[derive(Debug, PartialEq)]
pub struct Gem {
    id: u32,
    level: u32,
}

#[derive(Debug, PartialEq)]
pub struct Aura {
    caster: String,
    id: u32,
}

pub fn combatant_info(input: &str) -> IResult<&str, CombatantInfo> {
    let (input, (guid, _, strength, agility, stamina)) =
        tuple((take_until(","), tag(","), num, num, num))(input)?;

    let (input, (intelligence, spirit, dodge, parry, block)) =
        tuple((num, num, num, num, num))(input)?;

    let (input, (critmelee, critranged, critspell, speed, lifesteal)) =
        tuple((num, num, num, num, num))(input)?;

    let (input, (hastemelee, hasteranged, hastespell, avoidance, mastery)) =
        tuple((num, num, num, num, num))(input)?;

    let (input, (versatilitydamagedone, versatilityhealingdone, versatilitydamagetaken, armor)) =
        tuple((num, num, num, num))(input)?;

    let (input, (currentspecid, _class_talent, _pvp_talents, _artifact_traits)) =
        tuple((num, tag("(),"), tag("(0,0,0,0),"), tag("[],")))(input)?;

    let (input, (_, (_, items), _)) =
        tuple((tag("["), map_res(take_until("]"), parse_items), tag("],")))(input)?;

    let combatant_info = CombatantInfo {
        guid: String::from(guid),
        strength,
        agility,
        stamina,
        intelligence,
        spirit,
        dodge,
        parry,
        block,
        critmelee,
        critranged,
        critspell,
        speed,
        lifesteal,
        hastemelee,
        hasteranged,
        hastespell,
        avoidance,
        mastery,
        versatilitydamagedone,
        versatilityhealingdone,
        versatilitydamagetaken,
        armor,
        currentspecid,
        class_talents: vec![],
        pvp_talents: vec![],
        artifact_traits: vec![],
        items,
        interesting_auras: vec![],
    };

    Ok((input, combatant_info))
}

fn parse_items(input: &str) -> IResult<&str, Vec<Option<Item>>> {
    let (input, items) = many0(parse_item)(input)?;
    Ok((input, items))
}

fn parse_item(input: &str) -> IResult<&str, Option<Item>> {
    let (input, (_, id, level, _)) = tuple((tag("("), num, num, tag("(")))(input)?;

    let (input, permanent_enchant_id) = opt(num)(input)?;
    let (input, _) = opt(tag(","))(input)?;
    let (input, temporary_enchant_id) = opt(num)(input)?;
    let (input, _) = opt(tag(","))(input)?;
    let (input, on_use_spell_enchant_id) = opt(num)(input)?;
    let (input, _) = tag(")")(input)?;

    let (input, (_bonus_list_ids, _gems)) = tuple((tag(",()"), tag(",()")))(input)?;

    let (input, _) = opt(tag(","))(input)?;
    let (input, _) = opt(tag(")"))(input)?;
    let (input, _) = opt(tag(","))(input)?;

    if id == 0 {
        Ok((input, None))
    } else {
        Ok((
            input,
            Some(Item {
                id,
                level,
                permanent_enchant_id,
                temporary_enchant_id,
                on_use_spell_enchant_id,
                bonus_list_ids: vec![],
                gems: vec![],
            }),
        ))
    }
}

#[test]
fn can_parse_items() {
    assert_eq!(
        parse_items("(18372,62,(0,564,0),(),()),(5976,1,(),(),())"),
        Ok((
            "",
            vec![
                Some(Item {
                    id: 18372,
                    level: 62,
                    permanent_enchant_id: Some(0),
                    temporary_enchant_id: Some(564),
                    on_use_spell_enchant_id: Some(0),
                    bonus_list_ids: vec![],
                    gems: vec![]
                }),
                Some(Item {
                    id: 5976,
                    level: 1,
                    permanent_enchant_id: None,
                    temporary_enchant_id: None,
                    on_use_spell_enchant_id: None,
                    bonus_list_ids: vec![],
                    gems: vec![]
                })
            ]
        ))
    )
}

#[test]
fn empty_item_give_none() {
    assert_eq!(parse_items("(0,0,(),(),())"), Ok(("", vec![None])))
}

#[test]
fn parse_item_with_enchant() {
    assert_eq!(
        parse_item("(16929,76,(2544,0,0),(),())"),
        Ok((
            "",
            Some(Item {
                id: 16929,
                level: 76,
                permanent_enchant_id: Some(2544),
                temporary_enchant_id: Some(0),
                on_use_spell_enchant_id: Some(0),
                bonus_list_ids: vec![],
                gems: vec![]
            })
        ))
    )
}

#[test]
fn parse_item_without_enchant() {
    assert_eq!(
        parse_item("(4334,34,(),(),())"),
        Ok((
            "",
            Some(Item {
                id: 4334,
                level: 34,
                permanent_enchant_id: None,
                temporary_enchant_id: None,
                on_use_spell_enchant_id: None,
                bonus_list_ids: vec![],
                gems: vec![]
            })
        ))
    )
}
