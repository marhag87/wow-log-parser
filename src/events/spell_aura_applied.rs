use nom::bytes::complete::{is_not, tag, take_until};
use nom::combinator::opt;
use nom::sequence::tuple;
use nom::IResult;

#[derive(Debug, PartialEq)]
pub struct SpellAuraApplied {
    pub guid: String,
    pub name: String,
}

pub fn spell_aura_applied(input: &str) -> IResult<&str, SpellAuraApplied> {
    // Only care about guid and name right now, discard everything else
    let (input, (guid, _, _, _, name)) = tuple((
        take_until(","),
        opt(tag("\"")),
        tag(","),
        opt(tag("\"")),
        is_not("\"-,"),
    ))(input)?;

    Ok((
        input,
        SpellAuraApplied {
            guid: String::from(guid),
            name: String::from(name),
        },
    ))
}

#[test]
fn can_parse_spell_aura_applied() {
    assert_eq!(
        spell_aura_applied("Player-4475-0001D24F,\"Hoom-Shazzrah\",0x511,0x0,Player-4475-0001D24F,\"Hoom-Shazzrah\",0x511,0x0,71,\"Defensive Stance\",0x1,BUFF"),
        Ok((
            "-Shazzrah\",0x511,0x0,Player-4475-0001D24F,\"Hoom-Shazzrah\",0x511,0x0,71,\"Defensive Stance\",0x1,BUFF",
            SpellAuraApplied {
                guid: String::from("Player-4475-0001D24F"),
                name: String::from("Hoom"),
            }
        ))
    )
}

#[test]
fn can_parse_creature_targets() {
    assert_eq!(
        spell_aura_applied("Creature-0-4480-429-21629-14323-0000516406,\"Guard Slip'kik\",0xa28,0x0,Player-4475-01231D7D,\"Sylver-Shazzrah\",0x518,0x0,22820,\"Slip'kik's Savvy\",0x8,BUFF"),
        Ok((
            "\",0xa28,0x0,Player-4475-01231D7D,\"Sylver-Shazzrah\",0x518,0x0,22820,\"Slip'kik's Savvy\",0x8,BUFF",
            SpellAuraApplied {
                guid: String::from("Creature-0-4480-429-21629-14323-0000516406"),
                name: String::from("Guard Slip'kik"),
            }
        ))
    )
}

#[test]
fn can_parse_unknown_caster() {
    assert_eq!(
        spell_aura_applied("0000000000000000,nil,0x80000000,0x80000000,Player-4475-01FD92C5,\"Kokosik-Shazzrah\",0x518,0x0,16609,\"Warchief's Blessing\",0x1,BUFF"),
        Ok((
            ",0x80000000,0x80000000,Player-4475-01FD92C5,\"Kokosik-Shazzrah\",0x518,0x0,16609,\"Warchief's Blessing\",0x1,BUFF",
            SpellAuraApplied {
                guid: String::from("0000000000000000"),
                name: String::from("nil"),
            }
        ))
    )
}

#[test]
fn can_pare_pets() {
    assert_eq!(
        spell_aura_applied("Pet-0-4480-249-2558-9696-01001B5790,\"Fang\",0x1114,0x0,Pet-0-4480-249-2558-9696-01001B5790,\"Fang\",0x1114,0x0,24597,\"Furious Howl\",0x1,BUFF"),
        Ok((
            "\",0x1114,0x0,Pet-0-4480-249-2558-9696-01001B5790,\"Fang\",0x1114,0x0,24597,\"Furious Howl\",0x1,BUFF",
            SpellAuraApplied {
                guid: String::from("Pet-0-4480-249-2558-9696-01001B5790"),
                name: String::from("Fang"),
            }
        ))
    )
}
