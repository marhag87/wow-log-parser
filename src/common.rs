use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::{map_res, opt};
use nom::IResult;
use std::num::ParseIntError;
use std::str::FromStr;

pub fn parse_u32(input: &str) -> Result<u32, ParseIntError> {
    u32::from_str(input)
}

pub fn num(input: &str) -> IResult<&str, u32> {
    let (input, result) = map_res(digit1, parse_u32)(input)?;
    let (input, _) = opt(tag(","))(input)?;
    Ok((input, result))
}
