use crate::common::parse_u32;
use chrono::{Datelike, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::{map_res, opt};
use nom::IResult;

pub fn datetime(input: &str) -> IResult<&str, NaiveDateTime> {
    let (input, month) = map_res(digit1, parse_u32)(input)?;
    let (input, _) = tag("/")(input)?;
    let (input, day) = map_res(digit1, parse_u32)(input)?;
    let date = NaiveDate::from_ymd(
        Utc::now().year(), // FIXME: Handle year
        month,
        day,
    );
    let (input, _) = tag(" ")(input)?;
    let (input, hour) = map_res(digit1, parse_u32)(input)?;
    let (input, _) = tag(":")(input)?;
    let (input, min) = map_res(digit1, parse_u32)(input)?;
    let (input, _) = tag(":")(input)?;
    let (input, sec) = map_res(digit1, parse_u32)(input)?;
    let (input, _) = tag(".")(input)?;
    let (input, milli) = map_res(digit1, parse_u32)(input)?;
    let (input, _) = opt(tag(" "))(input)?;
    let (input, _) = opt(tag(" "))(input)?;
    let time = NaiveTime::from_hms_milli(hour, min, sec, milli);
    Ok((input, NaiveDateTime::new(date, time)))
}

#[test]
fn parse_date() {
    assert_eq!(
        datetime("11/14 18:49:37.901 "),
        Ok((
            "",
            NaiveDateTime::new(
                NaiveDate::from_ymd(2019, 11, 14),
                NaiveTime::from_hms_milli(18, 49, 37, 901)
            )
        ))
    )
}
